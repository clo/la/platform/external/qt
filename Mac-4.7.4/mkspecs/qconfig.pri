#configuration
CONFIG +=  def_files_disabled exceptions no_mocdepend release stl qt_no_framework
QT_ARCH = macosx
QT_EDITION = OpenSource
QT_CONFIG +=  minimal-config small-config medium-config large-config full-config dwarf2 accessibility opengl reduce_exports ipv6 getaddrinfo ipv6ifname getifaddrs png no-freetype system-zlib nis iconv openssl xmlpatterns audio-backend svg release  x86 ppc

#versioning
QT_VERSION = 4.7.4
QT_MAJOR_VERSION = 4
QT_MINOR_VERSION = 7
QT_PATCH_VERSION = 4

#namespaces
QT_LIBINFIX = 
QT_NAMESPACE = 
QT_NAMESPACE_MAC_CRC = 

QMAKE_RPATHDIR += "/usr/local/Trolltech/Qt-4.7.4/lib"
QT_GCC_MAJOR_VERSION = 4
QT_GCC_MINOR_VERSION = 2
QT_GCC_PATCH_VERSION = 1
