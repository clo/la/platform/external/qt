CONFIG+= debug shared stl exceptions mmx 3dnow sse
QT_ARCH = windows
QT_EDITION = OpenSource
QT_CONFIG += release debug zlib no-tiff png accessibility s60 openssl-linked ipv6 native-gestures svg minimal-config small-config medium-config large-config full-config
#versioning 
QT_VERSION = 4.7.4
QT_MAJOR_VERSION = 4
QT_MINOR_VERSION = 7
QT_PATCH_VERSION = 4
#Qt for Windows CE c-runtime deployment
QT_CE_C_RUNTIME = no
#Qt for Symbian FPU settings
