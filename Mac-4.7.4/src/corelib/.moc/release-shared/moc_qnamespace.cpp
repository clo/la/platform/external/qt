/****************************************************************************
** Meta object code from reading C++ file 'qnamespace.h'
**
** Created: Mon Jul 16 17:04:05 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../global/qnamespace.h"
#include <QtCore/qobject.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qnamespace.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Qt[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
      61,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       3, 0x0,   20,  258,
      15, 0x1,    8,  298,
      33, 0x1,    8,  314,
      46, 0x0,    2,  330,
      58, 0x1,    2,  334,
      71, 0x0,    5,  338,
      83, 0x0,    2,  348,
      93, 0x1,   13,  352,
     103, 0x0,    4,  378,
     117, 0x0,   33,  386,
     128, 0x1,   33,  452,
     140, 0x0,    5,  518,
     152, 0x1,    5,  528,
     165, 0x0,  117,  538,
     181, 0x0,   11,  772,
     202, 0x1,   19,  794,
     223, 0x0,    2,  832,
     230, 0x0,  418,  836,
     234, 0x0,    5, 1672,
     244, 0x0,    7, 1682,
     253, 0x0,    4, 1696,
     265, 0x0,    5, 1704,
     278, 0x0,   19, 1714,
     289, 0x0,    2, 1752,
     298, 0x0,   25, 1756,
     310, 0x0,    4, 1806,
     321, 0x0,    3, 1814,
     337, 0x0,    7, 1820,
     352, 0x1,    7, 1834,
     368, 0x0,    7, 1848,
     380, 0x1,    7, 1862,
     393, 0x0,    9, 1876,
     404, 0x0,    3, 1894,
     413, 0x0,    7, 1900,
     423, 0x0,    3, 1914,
     439, 0x0,    2, 1920,
     455, 0x0,    4, 1924,
     462, 0x0,    6, 1932,
     477, 0x0,    4, 1944,
     493, 0x0,    2, 1952,
     502, 0x0,    2, 1956,
     511, 0x0,    4, 1960,
     525, 0x0,    4, 1968,
     543, 0x0,    2, 1976,
     562, 0x0,    3, 1980,
     567, 0x0,    5, 1986,
     585, 0x0,   15, 1996,
     601, 0x1,   15, 2026,
     618, 0x0,    5, 2056,
     634, 0x0,    3, 2066,
     650, 0x0,    6, 2072,
     661, 0x1,    6, 2084,
     673, 0x0,    3, 2096,
     684, 0x1,    8, 2102,
     694, 0x1,   10, 2118,
     705, 0x0,    3, 2138,
     720, 0x0,    8, 2144,
     740, 0x1,    8, 2160,
     761, 0x0,    5, 2176,
     770, 0x0,    5, 2186,
     783, 0x0,    7, 2196,

 // enum data: key, value
     795, uint(Qt::color0),
     802, uint(Qt::color1),
     809, uint(Qt::black),
     815, uint(Qt::white),
     821, uint(Qt::darkGray),
     830, uint(Qt::gray),
     835, uint(Qt::lightGray),
     845, uint(Qt::red),
     849, uint(Qt::green),
     855, uint(Qt::blue),
     860, uint(Qt::cyan),
     865, uint(Qt::magenta),
     873, uint(Qt::yellow),
     880, uint(Qt::darkRed),
     888, uint(Qt::darkGreen),
     898, uint(Qt::darkBlue),
     907, uint(Qt::darkCyan),
     916, uint(Qt::darkMagenta),
     928, uint(Qt::darkYellow),
     939, uint(Qt::transparent),
     951, uint(Qt::NoModifier),
     962, uint(Qt::ShiftModifier),
     976, uint(Qt::ControlModifier),
     992, uint(Qt::AltModifier),
    1004, uint(Qt::MetaModifier),
    1017, uint(Qt::KeypadModifier),
    1032, uint(Qt::GroupSwitchModifier),
    1052, uint(Qt::KeyboardModifierMask),
    1073, uint(Qt::NoButton),
    1082, uint(Qt::LeftButton),
    1093, uint(Qt::RightButton),
    1105, uint(Qt::MidButton),
    1115, uint(Qt::MiddleButton),
    1128, uint(Qt::XButton1),
    1137, uint(Qt::XButton2),
    1146, uint(Qt::MouseButtonMask),
    1162, uint(Qt::Horizontal),
    1173, uint(Qt::Vertical),
    1162, uint(Qt::Horizontal),
    1173, uint(Qt::Vertical),
    1182, uint(Qt::NoFocus),
    1190, uint(Qt::TabFocus),
    1199, uint(Qt::ClickFocus),
    1210, uint(Qt::StrongFocus),
    1222, uint(Qt::WheelFocus),
    1233, uint(Qt::AscendingOrder),
    1248, uint(Qt::DescendingOrder),
    1264, uint(Qt::AlignLeft),
    1274, uint(Qt::AlignLeading),
    1287, uint(Qt::AlignRight),
    1298, uint(Qt::AlignTrailing),
    1312, uint(Qt::AlignHCenter),
    1325, uint(Qt::AlignJustify),
    1338, uint(Qt::AlignAbsolute),
    1352, uint(Qt::AlignHorizontal_Mask),
    1373, uint(Qt::AlignTop),
    1382, uint(Qt::AlignBottom),
    1394, uint(Qt::AlignVCenter),
    1407, uint(Qt::AlignVertical_Mask),
    1426, uint(Qt::AlignCenter),
    1438, uint(Qt::ElideLeft),
    1448, uint(Qt::ElideRight),
    1459, uint(Qt::ElideMiddle),
    1471, uint(Qt::ElideNone),
    1481, uint(Qt::Widget),
    1488, uint(Qt::Window),
    1495, uint(Qt::Dialog),
    1502, uint(Qt::Sheet),
    1508, uint(Qt::Drawer),
    1515, uint(Qt::Popup),
    1521, uint(Qt::Tool),
    1526, uint(Qt::ToolTip),
    1534, uint(Qt::SplashScreen),
    1547, uint(Qt::Desktop),
    1555, uint(Qt::SubWindow),
    1565, uint(Qt::WindowType_Mask),
    1581, uint(Qt::MSWindowsFixedSizeDialogHint),
    1610, uint(Qt::MSWindowsOwnDC),
    1625, uint(Qt::X11BypassWindowManagerHint),
    1652, uint(Qt::FramelessWindowHint),
    1672, uint(Qt::WindowTitleHint),
    1688, uint(Qt::WindowSystemMenuHint),
    1709, uint(Qt::WindowMinimizeButtonHint),
    1734, uint(Qt::WindowMaximizeButtonHint),
    1759, uint(Qt::WindowMinMaxButtonsHint),
    1783, uint(Qt::WindowContextHelpButtonHint),
    1811, uint(Qt::WindowShadeButtonHint),
    1833, uint(Qt::WindowStaysOnTopHint),
    1854, uint(Qt::CustomizeWindowHint),
    1874, uint(Qt::WindowStaysOnBottomHint),
    1898, uint(Qt::WindowCloseButtonHint),
    1920, uint(Qt::MacWindowToolBarButtonHint),
    1947, uint(Qt::BypassGraphicsProxyWidget),
    1973, uint(Qt::WindowOkButtonHint),
    1992, uint(Qt::WindowCancelButtonHint),
    2015, uint(Qt::WindowSoftkeysVisibleHint),
    2041, uint(Qt::WindowSoftkeysRespondHint),
    1481, uint(Qt::Widget),
    1488, uint(Qt::Window),
    1495, uint(Qt::Dialog),
    1502, uint(Qt::Sheet),
    1508, uint(Qt::Drawer),
    1515, uint(Qt::Popup),
    1521, uint(Qt::Tool),
    1526, uint(Qt::ToolTip),
    1534, uint(Qt::SplashScreen),
    1547, uint(Qt::Desktop),
    1555, uint(Qt::SubWindow),
    1565, uint(Qt::WindowType_Mask),
    1581, uint(Qt::MSWindowsFixedSizeDialogHint),
    1610, uint(Qt::MSWindowsOwnDC),
    1625, uint(Qt::X11BypassWindowManagerHint),
    1652, uint(Qt::FramelessWindowHint),
    1672, uint(Qt::WindowTitleHint),
    1688, uint(Qt::WindowSystemMenuHint),
    1709, uint(Qt::WindowMinimizeButtonHint),
    1734, uint(Qt::WindowMaximizeButtonHint),
    1759, uint(Qt::WindowMinMaxButtonsHint),
    1783, uint(Qt::WindowContextHelpButtonHint),
    1811, uint(Qt::WindowShadeButtonHint),
    1833, uint(Qt::WindowStaysOnTopHint),
    1854, uint(Qt::CustomizeWindowHint),
    1874, uint(Qt::WindowStaysOnBottomHint),
    1898, uint(Qt::WindowCloseButtonHint),
    1920, uint(Qt::MacWindowToolBarButtonHint),
    1947, uint(Qt::BypassGraphicsProxyWidget),
    1973, uint(Qt::WindowOkButtonHint),
    1992, uint(Qt::WindowCancelButtonHint),
    2015, uint(Qt::WindowSoftkeysVisibleHint),
    2041, uint(Qt::WindowSoftkeysRespondHint),
    2067, uint(Qt::WindowNoState),
    2081, uint(Qt::WindowMinimized),
    2097, uint(Qt::WindowMaximized),
    2113, uint(Qt::WindowFullScreen),
    2130, uint(Qt::WindowActive),
    2067, uint(Qt::WindowNoState),
    2081, uint(Qt::WindowMinimized),
    2097, uint(Qt::WindowMaximized),
    2113, uint(Qt::WindowFullScreen),
    2130, uint(Qt::WindowActive),
    2143, uint(Qt::WA_Disabled),
    2155, uint(Qt::WA_UnderMouse),
    2169, uint(Qt::WA_MouseTracking),
    2186, uint(Qt::WA_ContentsPropagated),
    2208, uint(Qt::WA_OpaquePaintEvent),
    2228, uint(Qt::WA_NoBackground),
    2244, uint(Qt::WA_StaticContents),
    2262, uint(Qt::WA_LaidOut),
    2273, uint(Qt::WA_PaintOnScreen),
    2290, uint(Qt::WA_NoSystemBackground),
    2312, uint(Qt::WA_UpdatesDisabled),
    2331, uint(Qt::WA_Mapped),
    2341, uint(Qt::WA_MacNoClickThrough),
    2362, uint(Qt::WA_PaintOutsidePaintEvent),
    2388, uint(Qt::WA_InputMethodEnabled),
    2410, uint(Qt::WA_WState_Visible),
    2428, uint(Qt::WA_WState_Hidden),
    2445, uint(Qt::WA_ForceDisabled),
    2462, uint(Qt::WA_KeyCompression),
    2480, uint(Qt::WA_PendingMoveEvent),
    2500, uint(Qt::WA_PendingResizeEvent),
    2522, uint(Qt::WA_SetPalette),
    2536, uint(Qt::WA_SetFont),
    2547, uint(Qt::WA_SetCursor),
    2560, uint(Qt::WA_NoChildEventsFromChildren),
    2589, uint(Qt::WA_WindowModified),
    2607, uint(Qt::WA_Resized),
    2618, uint(Qt::WA_Moved),
    2627, uint(Qt::WA_PendingUpdate),
    2644, uint(Qt::WA_InvalidSize),
    2659, uint(Qt::WA_MacBrushedMetal),
    2678, uint(Qt::WA_MacMetalStyle),
    2695, uint(Qt::WA_CustomWhatsThis),
    2714, uint(Qt::WA_LayoutOnEntireRect),
    2736, uint(Qt::WA_OutsideWSRange),
    2754, uint(Qt::WA_GrabbedShortcut),
    2773, uint(Qt::WA_TransparentForMouseEvents),
    2802, uint(Qt::WA_PaintUnclipped),
    2820, uint(Qt::WA_SetWindowIcon),
    2837, uint(Qt::WA_NoMouseReplay),
    2854, uint(Qt::WA_DeleteOnClose),
    2871, uint(Qt::WA_RightToLeft),
    2886, uint(Qt::WA_SetLayoutDirection),
    2908, uint(Qt::WA_NoChildEventsForParent),
    2934, uint(Qt::WA_ForceUpdatesDisabled),
    2958, uint(Qt::WA_WState_Created),
    2976, uint(Qt::WA_WState_CompressKeys),
    2999, uint(Qt::WA_WState_InPaintEvent),
    3022, uint(Qt::WA_WState_Reparented),
    3043, uint(Qt::WA_WState_ConfigPending),
    3067, uint(Qt::WA_WState_Polished),
    3086, uint(Qt::WA_WState_DND),
    3100, uint(Qt::WA_WState_OwnSizePolicy),
    3124, uint(Qt::WA_WState_ExplicitShowHide),
    3151, uint(Qt::WA_ShowModal),
    3164, uint(Qt::WA_MouseNoMask),
    3179, uint(Qt::WA_GroupLeader),
    3194, uint(Qt::WA_NoMousePropagation),
    3216, uint(Qt::WA_Hover),
    3225, uint(Qt::WA_InputMethodTransparent),
    3251, uint(Qt::WA_QuitOnClose),
    3266, uint(Qt::WA_KeyboardFocusChange),
    3289, uint(Qt::WA_AcceptDrops),
    3304, uint(Qt::WA_DropSiteRegistered),
    3326, uint(Qt::WA_ForceAcceptDrops),
    3346, uint(Qt::WA_WindowPropagation),
    3367, uint(Qt::WA_NoX11EventCompression),
    3392, uint(Qt::WA_TintedBackground),
    3412, uint(Qt::WA_X11OpenGLOverlay),
    3432, uint(Qt::WA_AlwaysShowToolTips),
    3454, uint(Qt::WA_MacOpaqueSizeGrip),
    3475, uint(Qt::WA_SetStyle),
    3487, uint(Qt::WA_SetLocale),
    3500, uint(Qt::WA_MacShowFocusRect),
    3520, uint(Qt::WA_MacNormalSize),
    3537, uint(Qt::WA_MacSmallSize),
    3553, uint(Qt::WA_MacMiniSize),
    3568, uint(Qt::WA_LayoutUsesWidgetRect),
    3592, uint(Qt::WA_StyledBackground),
    3612, uint(Qt::WA_MSWindowsUseDirect3D),
    3636, uint(Qt::WA_CanHostQMdiSubWindowTitleBar),
    3668, uint(Qt::WA_MacAlwaysShowToolWindow),
    3695, uint(Qt::WA_StyleSheet),
    3709, uint(Qt::WA_ShowWithoutActivating),
    3734, uint(Qt::WA_X11BypassTransientForHint),
    3763, uint(Qt::WA_NativeWindow),
    3779, uint(Qt::WA_DontCreateNativeAncestors),
    3808, uint(Qt::WA_MacVariableSize),
    3827, uint(Qt::WA_DontShowOnScreen),
    3847, uint(Qt::WA_X11NetWmWindowTypeDesktop),
    3876, uint(Qt::WA_X11NetWmWindowTypeDock),
    3902, uint(Qt::WA_X11NetWmWindowTypeToolBar),
    3931, uint(Qt::WA_X11NetWmWindowTypeMenu),
    3957, uint(Qt::WA_X11NetWmWindowTypeUtility),
    3986, uint(Qt::WA_X11NetWmWindowTypeSplash),
    4014, uint(Qt::WA_X11NetWmWindowTypeDialog),
    4042, uint(Qt::WA_X11NetWmWindowTypeDropDownMenu),
    4076, uint(Qt::WA_X11NetWmWindowTypePopupMenu),
    4107, uint(Qt::WA_X11NetWmWindowTypeToolTip),
    4136, uint(Qt::WA_X11NetWmWindowTypeNotification),
    4170, uint(Qt::WA_X11NetWmWindowTypeCombo),
    4197, uint(Qt::WA_X11NetWmWindowTypeDND),
    4222, uint(Qt::WA_MacFrameworkScaled),
    4244, uint(Qt::WA_SetWindowModality),
    4265, uint(Qt::WA_WState_WindowOpacitySet),
    4292, uint(Qt::WA_TranslucentBackground),
    4317, uint(Qt::WA_AcceptTouchEvents),
    4338, uint(Qt::WA_WState_AcceptedTouchBeginEvent),
    4372, uint(Qt::WA_TouchPadAcceptSingleTouchEvents),
    4407, uint(Qt::WA_MergeSoftkeys),
    4424, uint(Qt::WA_MergeSoftkeysRecursively),
    4452, uint(Qt::WA_LockPortraitOrientation),
    4479, uint(Qt::WA_LockLandscapeOrientation),
    4507, uint(Qt::WA_AutoOrientation),
    4526, uint(Qt::WA_X11DoNotAcceptFocus),
    4549, uint(Qt::WA_SymbianNoSystemRotation),
    4576, uint(Qt::WA_AttributeCount),
    4594, uint(Qt::AA_ImmediateWidgetCreation),
    4621, uint(Qt::AA_MSWindowsUseDirect3DByDefault),
    4654, uint(Qt::AA_DontShowIconsInMenus),
    4678, uint(Qt::AA_NativeWindows),
    4695, uint(Qt::AA_DontCreateNativeWidgetSiblings),
    4729, uint(Qt::AA_MacPluginApplication),
    4753, uint(Qt::AA_DontUseNativeMenuBar),
    4777, uint(Qt::AA_MacDontSwapCtrlAndMeta),
    4803, uint(Qt::AA_S60DontConstructApplicationPanes),
    4839, uint(Qt::AA_S60DisablePartialScreenInputMode),
    4875, uint(Qt::AA_AttributeCount),
    4893, uint(Qt::ColorMode_Mask),
    4908, uint(Qt::AutoColor),
    4918, uint(Qt::ColorOnly),
    4928, uint(Qt::MonoOnly),
    4937, uint(Qt::AlphaDither_Mask),
    4954, uint(Qt::ThresholdAlphaDither),
    4975, uint(Qt::OrderedAlphaDither),
    4994, uint(Qt::DiffuseAlphaDither),
    5013, uint(Qt::NoAlpha),
    5021, uint(Qt::Dither_Mask),
    5033, uint(Qt::DiffuseDither),
    5047, uint(Qt::OrderedDither),
    5061, uint(Qt::ThresholdDither),
    5077, uint(Qt::DitherMode_Mask),
    5093, uint(Qt::AutoDither),
    5104, uint(Qt::PreferDither),
    5117, uint(Qt::AvoidDither),
    5129, uint(Qt::NoOpaqueDetection),
    5147, uint(Qt::NoFormatConversion),
    5166, uint(Qt::TransparentMode),
    5182, uint(Qt::OpaqueMode),
    5193, uint(Qt::Key_Escape),
    5204, uint(Qt::Key_Tab),
    5212, uint(Qt::Key_Backtab),
    5224, uint(Qt::Key_Backspace),
    5238, uint(Qt::Key_Return),
    5249, uint(Qt::Key_Enter),
    5259, uint(Qt::Key_Insert),
    5270, uint(Qt::Key_Delete),
    5281, uint(Qt::Key_Pause),
    5291, uint(Qt::Key_Print),
    5301, uint(Qt::Key_SysReq),
    5312, uint(Qt::Key_Clear),
    5322, uint(Qt::Key_Home),
    5331, uint(Qt::Key_End),
    5339, uint(Qt::Key_Left),
    5348, uint(Qt::Key_Up),
    5355, uint(Qt::Key_Right),
    5365, uint(Qt::Key_Down),
    5374, uint(Qt::Key_PageUp),
    5385, uint(Qt::Key_PageDown),
    5398, uint(Qt::Key_Shift),
    5408, uint(Qt::Key_Control),
    5420, uint(Qt::Key_Meta),
    5429, uint(Qt::Key_Alt),
    5437, uint(Qt::Key_CapsLock),
    5450, uint(Qt::Key_NumLock),
    5462, uint(Qt::Key_ScrollLock),
    5477, uint(Qt::Key_F1),
    5484, uint(Qt::Key_F2),
    5491, uint(Qt::Key_F3),
    5498, uint(Qt::Key_F4),
    5505, uint(Qt::Key_F5),
    5512, uint(Qt::Key_F6),
    5519, uint(Qt::Key_F7),
    5526, uint(Qt::Key_F8),
    5533, uint(Qt::Key_F9),
    5540, uint(Qt::Key_F10),
    5548, uint(Qt::Key_F11),
    5556, uint(Qt::Key_F12),
    5564, uint(Qt::Key_F13),
    5572, uint(Qt::Key_F14),
    5580, uint(Qt::Key_F15),
    5588, uint(Qt::Key_F16),
    5596, uint(Qt::Key_F17),
    5604, uint(Qt::Key_F18),
    5612, uint(Qt::Key_F19),
    5620, uint(Qt::Key_F20),
    5628, uint(Qt::Key_F21),
    5636, uint(Qt::Key_F22),
    5644, uint(Qt::Key_F23),
    5652, uint(Qt::Key_F24),
    5660, uint(Qt::Key_F25),
    5668, uint(Qt::Key_F26),
    5676, uint(Qt::Key_F27),
    5684, uint(Qt::Key_F28),
    5692, uint(Qt::Key_F29),
    5700, uint(Qt::Key_F30),
    5708, uint(Qt::Key_F31),
    5716, uint(Qt::Key_F32),
    5724, uint(Qt::Key_F33),
    5732, uint(Qt::Key_F34),
    5740, uint(Qt::Key_F35),
    5748, uint(Qt::Key_Super_L),
    5760, uint(Qt::Key_Super_R),
    5772, uint(Qt::Key_Menu),
    5781, uint(Qt::Key_Hyper_L),
    5793, uint(Qt::Key_Hyper_R),
    5805, uint(Qt::Key_Help),
    5814, uint(Qt::Key_Direction_L),
    5830, uint(Qt::Key_Direction_R),
    5846, uint(Qt::Key_Space),
    5856, uint(Qt::Key_Any),
    5864, uint(Qt::Key_Exclam),
    5875, uint(Qt::Key_QuoteDbl),
    5888, uint(Qt::Key_NumberSign),
    5903, uint(Qt::Key_Dollar),
    5914, uint(Qt::Key_Percent),
    5926, uint(Qt::Key_Ampersand),
    5940, uint(Qt::Key_Apostrophe),
    5955, uint(Qt::Key_ParenLeft),
    5969, uint(Qt::Key_ParenRight),
    5984, uint(Qt::Key_Asterisk),
    5997, uint(Qt::Key_Plus),
    6006, uint(Qt::Key_Comma),
    6016, uint(Qt::Key_Minus),
    6026, uint(Qt::Key_Period),
    6037, uint(Qt::Key_Slash),
    6047, uint(Qt::Key_0),
    6053, uint(Qt::Key_1),
    6059, uint(Qt::Key_2),
    6065, uint(Qt::Key_3),
    6071, uint(Qt::Key_4),
    6077, uint(Qt::Key_5),
    6083, uint(Qt::Key_6),
    6089, uint(Qt::Key_7),
    6095, uint(Qt::Key_8),
    6101, uint(Qt::Key_9),
    6107, uint(Qt::Key_Colon),
    6117, uint(Qt::Key_Semicolon),
    6131, uint(Qt::Key_Less),
    6140, uint(Qt::Key_Equal),
    6150, uint(Qt::Key_Greater),
    6162, uint(Qt::Key_Question),
    6175, uint(Qt::Key_At),
    6182, uint(Qt::Key_A),
    6188, uint(Qt::Key_B),
    6194, uint(Qt::Key_C),
    6200, uint(Qt::Key_D),
    6206, uint(Qt::Key_E),
    6212, uint(Qt::Key_F),
    6218, uint(Qt::Key_G),
    6224, uint(Qt::Key_H),
    6230, uint(Qt::Key_I),
    6236, uint(Qt::Key_J),
    6242, uint(Qt::Key_K),
    6248, uint(Qt::Key_L),
    6254, uint(Qt::Key_M),
    6260, uint(Qt::Key_N),
    6266, uint(Qt::Key_O),
    6272, uint(Qt::Key_P),
    6278, uint(Qt::Key_Q),
    6284, uint(Qt::Key_R),
    6290, uint(Qt::Key_S),
    6296, uint(Qt::Key_T),
    6302, uint(Qt::Key_U),
    6308, uint(Qt::Key_V),
    6314, uint(Qt::Key_W),
    6320, uint(Qt::Key_X),
    6326, uint(Qt::Key_Y),
    6332, uint(Qt::Key_Z),
    6338, uint(Qt::Key_BracketLeft),
    6354, uint(Qt::Key_Backslash),
    6368, uint(Qt::Key_BracketRight),
    6385, uint(Qt::Key_AsciiCircum),
    6401, uint(Qt::Key_Underscore),
    6416, uint(Qt::Key_QuoteLeft),
    6430, uint(Qt::Key_BraceLeft),
    6444, uint(Qt::Key_Bar),
    6452, uint(Qt::Key_BraceRight),
    6467, uint(Qt::Key_AsciiTilde),
    6482, uint(Qt::Key_nobreakspace),
    6499, uint(Qt::Key_exclamdown),
    6514, uint(Qt::Key_cent),
    6523, uint(Qt::Key_sterling),
    6536, uint(Qt::Key_currency),
    6549, uint(Qt::Key_yen),
    6557, uint(Qt::Key_brokenbar),
    6571, uint(Qt::Key_section),
    6583, uint(Qt::Key_diaeresis),
    6597, uint(Qt::Key_copyright),
    6611, uint(Qt::Key_ordfeminine),
    6627, uint(Qt::Key_guillemotleft),
    6645, uint(Qt::Key_notsign),
    6657, uint(Qt::Key_hyphen),
    6668, uint(Qt::Key_registered),
    6683, uint(Qt::Key_macron),
    6694, uint(Qt::Key_degree),
    6705, uint(Qt::Key_plusminus),
    6719, uint(Qt::Key_twosuperior),
    6735, uint(Qt::Key_threesuperior),
    6753, uint(Qt::Key_acute),
    6763, uint(Qt::Key_mu),
    6770, uint(Qt::Key_paragraph),
    6784, uint(Qt::Key_periodcentered),
    6803, uint(Qt::Key_cedilla),
    6815, uint(Qt::Key_onesuperior),
    6831, uint(Qt::Key_masculine),
    6845, uint(Qt::Key_guillemotright),
    6864, uint(Qt::Key_onequarter),
    6879, uint(Qt::Key_onehalf),
    6891, uint(Qt::Key_threequarters),
    6909, uint(Qt::Key_questiondown),
    6926, uint(Qt::Key_Agrave),
    6937, uint(Qt::Key_Aacute),
    6948, uint(Qt::Key_Acircumflex),
    6964, uint(Qt::Key_Atilde),
    6975, uint(Qt::Key_Adiaeresis),
    6990, uint(Qt::Key_Aring),
    7000, uint(Qt::Key_AE),
    7007, uint(Qt::Key_Ccedilla),
    7020, uint(Qt::Key_Egrave),
    7031, uint(Qt::Key_Eacute),
    7042, uint(Qt::Key_Ecircumflex),
    7058, uint(Qt::Key_Ediaeresis),
    7073, uint(Qt::Key_Igrave),
    7084, uint(Qt::Key_Iacute),
    7095, uint(Qt::Key_Icircumflex),
    7111, uint(Qt::Key_Idiaeresis),
    7126, uint(Qt::Key_ETH),
    7134, uint(Qt::Key_Ntilde),
    7145, uint(Qt::Key_Ograve),
    7156, uint(Qt::Key_Oacute),
    7167, uint(Qt::Key_Ocircumflex),
    7183, uint(Qt::Key_Otilde),
    7194, uint(Qt::Key_Odiaeresis),
    7209, uint(Qt::Key_multiply),
    7222, uint(Qt::Key_Ooblique),
    7235, uint(Qt::Key_Ugrave),
    7246, uint(Qt::Key_Uacute),
    7257, uint(Qt::Key_Ucircumflex),
    7273, uint(Qt::Key_Udiaeresis),
    7288, uint(Qt::Key_Yacute),
    7299, uint(Qt::Key_THORN),
    7309, uint(Qt::Key_ssharp),
    7320, uint(Qt::Key_division),
    7333, uint(Qt::Key_ydiaeresis),
    7348, uint(Qt::Key_AltGr),
    7358, uint(Qt::Key_Multi_key),
    7372, uint(Qt::Key_Codeinput),
    7386, uint(Qt::Key_SingleCandidate),
    7406, uint(Qt::Key_MultipleCandidate),
    7428, uint(Qt::Key_PreviousCandidate),
    7450, uint(Qt::Key_Mode_switch),
    7466, uint(Qt::Key_Kanji),
    7476, uint(Qt::Key_Muhenkan),
    7489, uint(Qt::Key_Henkan),
    7500, uint(Qt::Key_Romaji),
    7511, uint(Qt::Key_Hiragana),
    7524, uint(Qt::Key_Katakana),
    7537, uint(Qt::Key_Hiragana_Katakana),
    7559, uint(Qt::Key_Zenkaku),
    7571, uint(Qt::Key_Hankaku),
    7583, uint(Qt::Key_Zenkaku_Hankaku),
    7603, uint(Qt::Key_Touroku),
    7615, uint(Qt::Key_Massyo),
    7626, uint(Qt::Key_Kana_Lock),
    7640, uint(Qt::Key_Kana_Shift),
    7655, uint(Qt::Key_Eisu_Shift),
    7670, uint(Qt::Key_Eisu_toggle),
    7686, uint(Qt::Key_Hangul),
    7697, uint(Qt::Key_Hangul_Start),
    7714, uint(Qt::Key_Hangul_End),
    7729, uint(Qt::Key_Hangul_Hanja),
    7746, uint(Qt::Key_Hangul_Jamo),
    7762, uint(Qt::Key_Hangul_Romaja),
    7780, uint(Qt::Key_Hangul_Jeonja),
    7798, uint(Qt::Key_Hangul_Banja),
    7815, uint(Qt::Key_Hangul_PreHanja),
    7835, uint(Qt::Key_Hangul_PostHanja),
    7856, uint(Qt::Key_Hangul_Special),
    7875, uint(Qt::Key_Dead_Grave),
    7890, uint(Qt::Key_Dead_Acute),
    7905, uint(Qt::Key_Dead_Circumflex),
    7925, uint(Qt::Key_Dead_Tilde),
    7940, uint(Qt::Key_Dead_Macron),
    7956, uint(Qt::Key_Dead_Breve),
    7971, uint(Qt::Key_Dead_Abovedot),
    7989, uint(Qt::Key_Dead_Diaeresis),
    8008, uint(Qt::Key_Dead_Abovering),
    8027, uint(Qt::Key_Dead_Doubleacute),
    8048, uint(Qt::Key_Dead_Caron),
    8063, uint(Qt::Key_Dead_Cedilla),
    8080, uint(Qt::Key_Dead_Ogonek),
    8096, uint(Qt::Key_Dead_Iota),
    8110, uint(Qt::Key_Dead_Voiced_Sound),
    8132, uint(Qt::Key_Dead_Semivoiced_Sound),
    8158, uint(Qt::Key_Dead_Belowdot),
    8176, uint(Qt::Key_Dead_Hook),
    8190, uint(Qt::Key_Dead_Horn),
    8204, uint(Qt::Key_Back),
    8213, uint(Qt::Key_Forward),
    8225, uint(Qt::Key_Stop),
    8234, uint(Qt::Key_Refresh),
    8246, uint(Qt::Key_VolumeDown),
    8261, uint(Qt::Key_VolumeMute),
    8276, uint(Qt::Key_VolumeUp),
    8289, uint(Qt::Key_BassBoost),
    8303, uint(Qt::Key_BassUp),
    8314, uint(Qt::Key_BassDown),
    8327, uint(Qt::Key_TrebleUp),
    8340, uint(Qt::Key_TrebleDown),
    8355, uint(Qt::Key_MediaPlay),
    8369, uint(Qt::Key_MediaStop),
    8383, uint(Qt::Key_MediaPrevious),
    8401, uint(Qt::Key_MediaNext),
    8415, uint(Qt::Key_MediaRecord),
    8431, uint(Qt::Key_MediaPause),
    8446, uint(Qt::Key_MediaTogglePlayPause),
    8471, uint(Qt::Key_HomePage),
    8484, uint(Qt::Key_Favorites),
    8498, uint(Qt::Key_Search),
    8509, uint(Qt::Key_Standby),
    8521, uint(Qt::Key_OpenUrl),
    8533, uint(Qt::Key_LaunchMail),
    8548, uint(Qt::Key_LaunchMedia),
    8564, uint(Qt::Key_Launch0),
    8576, uint(Qt::Key_Launch1),
    8588, uint(Qt::Key_Launch2),
    8600, uint(Qt::Key_Launch3),
    8612, uint(Qt::Key_Launch4),
    8624, uint(Qt::Key_Launch5),
    8636, uint(Qt::Key_Launch6),
    8648, uint(Qt::Key_Launch7),
    8660, uint(Qt::Key_Launch8),
    8672, uint(Qt::Key_Launch9),
    8684, uint(Qt::Key_LaunchA),
    8696, uint(Qt::Key_LaunchB),
    8708, uint(Qt::Key_LaunchC),
    8720, uint(Qt::Key_LaunchD),
    8732, uint(Qt::Key_LaunchE),
    8744, uint(Qt::Key_LaunchF),
    8756, uint(Qt::Key_MonBrightnessUp),
    8776, uint(Qt::Key_MonBrightnessDown),
    8798, uint(Qt::Key_KeyboardLightOnOff),
    8821, uint(Qt::Key_KeyboardBrightnessUp),
    8846, uint(Qt::Key_KeyboardBrightnessDown),
    8873, uint(Qt::Key_PowerOff),
    8886, uint(Qt::Key_WakeUp),
    8897, uint(Qt::Key_Eject),
    8907, uint(Qt::Key_ScreenSaver),
    8923, uint(Qt::Key_WWW),
    8931, uint(Qt::Key_Memo),
    8940, uint(Qt::Key_LightBulb),
    8954, uint(Qt::Key_Shop),
    8963, uint(Qt::Key_History),
    8975, uint(Qt::Key_AddFavorite),
    8991, uint(Qt::Key_HotLinks),
    9004, uint(Qt::Key_BrightnessAdjust),
    9025, uint(Qt::Key_Finance),
    9037, uint(Qt::Key_Community),
    9051, uint(Qt::Key_AudioRewind),
    9067, uint(Qt::Key_BackForward),
    9083, uint(Qt::Key_ApplicationLeft),
    9103, uint(Qt::Key_ApplicationRight),
    9124, uint(Qt::Key_Book),
    9133, uint(Qt::Key_CD),
    9140, uint(Qt::Key_Calculator),
    9155, uint(Qt::Key_ToDoList),
    9168, uint(Qt::Key_ClearGrab),
    9182, uint(Qt::Key_Close),
    9192, uint(Qt::Key_Copy),
    9201, uint(Qt::Key_Cut),
    9209, uint(Qt::Key_Display),
    9221, uint(Qt::Key_DOS),
    9229, uint(Qt::Key_Documents),
    9243, uint(Qt::Key_Excel),
    9253, uint(Qt::Key_Explorer),
    9266, uint(Qt::Key_Game),
    9275, uint(Qt::Key_Go),
    9282, uint(Qt::Key_iTouch),
    9293, uint(Qt::Key_LogOff),
    9304, uint(Qt::Key_Market),
    9315, uint(Qt::Key_Meeting),
    9327, uint(Qt::Key_MenuKB),
    9338, uint(Qt::Key_MenuPB),
    9349, uint(Qt::Key_MySites),
    9361, uint(Qt::Key_News),
    9370, uint(Qt::Key_OfficeHome),
    9385, uint(Qt::Key_Option),
    9396, uint(Qt::Key_Paste),
    9406, uint(Qt::Key_Phone),
    9416, uint(Qt::Key_Calendar),
    9429, uint(Qt::Key_Reply),
    9439, uint(Qt::Key_Reload),
    9450, uint(Qt::Key_RotateWindows),
    9468, uint(Qt::Key_RotationPB),
    9483, uint(Qt::Key_RotationKB),
    9498, uint(Qt::Key_Save),
    9507, uint(Qt::Key_Send),
    9516, uint(Qt::Key_Spell),
    9526, uint(Qt::Key_SplitScreen),
    9542, uint(Qt::Key_Support),
    9554, uint(Qt::Key_TaskPane),
    9567, uint(Qt::Key_Terminal),
    9580, uint(Qt::Key_Tools),
    9590, uint(Qt::Key_Travel),
    9601, uint(Qt::Key_Video),
    9611, uint(Qt::Key_Word),
    9620, uint(Qt::Key_Xfer),
    9629, uint(Qt::Key_ZoomIn),
    9640, uint(Qt::Key_ZoomOut),
    9652, uint(Qt::Key_Away),
    9661, uint(Qt::Key_Messenger),
    9675, uint(Qt::Key_WebCam),
    9686, uint(Qt::Key_MailForward),
    9702, uint(Qt::Key_Pictures),
    9715, uint(Qt::Key_Music),
    9725, uint(Qt::Key_Battery),
    9737, uint(Qt::Key_Bluetooth),
    9751, uint(Qt::Key_WLAN),
    9760, uint(Qt::Key_UWB),
    9768, uint(Qt::Key_AudioForward),
    9785, uint(Qt::Key_AudioRepeat),
    9801, uint(Qt::Key_AudioRandomPlay),
    9821, uint(Qt::Key_Subtitle),
    9834, uint(Qt::Key_AudioCycleTrack),
    9854, uint(Qt::Key_Time),
    9863, uint(Qt::Key_Hibernate),
    9877, uint(Qt::Key_View),
    9886, uint(Qt::Key_TopMenu),
    9898, uint(Qt::Key_PowerDown),
    9912, uint(Qt::Key_Suspend),
    9924, uint(Qt::Key_ContrastAdjust),
    9943, uint(Qt::Key_LaunchG),
    9955, uint(Qt::Key_LaunchH),
    9967, uint(Qt::Key_MediaLast),
    9981, uint(Qt::Key_Select),
    9992, uint(Qt::Key_Yes),
    10000, uint(Qt::Key_No),
    10007, uint(Qt::Key_Cancel),
    10018, uint(Qt::Key_Printer),
    10030, uint(Qt::Key_Execute),
    10042, uint(Qt::Key_Sleep),
    10052, uint(Qt::Key_Play),
    10061, uint(Qt::Key_Zoom),
    10070, uint(Qt::Key_Context1),
    10083, uint(Qt::Key_Context2),
    10096, uint(Qt::Key_Context3),
    10109, uint(Qt::Key_Context4),
    10122, uint(Qt::Key_Call),
    10131, uint(Qt::Key_Hangup),
    10142, uint(Qt::Key_Flip),
    10151, uint(Qt::Key_ToggleCallHangup),
    10172, uint(Qt::Key_VoiceDial),
    10186, uint(Qt::Key_LastNumberRedial),
    10207, uint(Qt::Key_Camera),
    10218, uint(Qt::Key_CameraFocus),
    10234, uint(Qt::Key_unknown),
    10246, uint(Qt::NoArrow),
    10254, uint(Qt::UpArrow),
    10262, uint(Qt::DownArrow),
    10272, uint(Qt::LeftArrow),
    10282, uint(Qt::RightArrow),
    10293, uint(Qt::NoPen),
    10299, uint(Qt::SolidLine),
    10309, uint(Qt::DashLine),
    10318, uint(Qt::DotLine),
    10326, uint(Qt::DashDotLine),
    10338, uint(Qt::DashDotDotLine),
    10353, uint(Qt::CustomDashLine),
    10368, uint(Qt::FlatCap),
    10376, uint(Qt::SquareCap),
    10386, uint(Qt::RoundCap),
    10395, uint(Qt::MPenCapStyle),
    10408, uint(Qt::MiterJoin),
    10418, uint(Qt::BevelJoin),
    10428, uint(Qt::RoundJoin),
    10438, uint(Qt::SvgMiterJoin),
    10451, uint(Qt::MPenJoinStyle),
    10465, uint(Qt::NoBrush),
    10473, uint(Qt::SolidPattern),
    10486, uint(Qt::Dense1Pattern),
    10500, uint(Qt::Dense2Pattern),
    10514, uint(Qt::Dense3Pattern),
    10528, uint(Qt::Dense4Pattern),
    10542, uint(Qt::Dense5Pattern),
    10556, uint(Qt::Dense6Pattern),
    10570, uint(Qt::Dense7Pattern),
    10584, uint(Qt::HorPattern),
    10595, uint(Qt::VerPattern),
    10606, uint(Qt::CrossPattern),
    10619, uint(Qt::BDiagPattern),
    10632, uint(Qt::FDiagPattern),
    10645, uint(Qt::DiagCrossPattern),
    10662, uint(Qt::LinearGradientPattern),
    10684, uint(Qt::RadialGradientPattern),
    10706, uint(Qt::ConicalGradientPattern),
    10729, uint(Qt::TexturePattern),
    10744, uint(Qt::AbsoluteSize),
    10757, uint(Qt::RelativeSize),
    10770, uint(Qt::ArrowCursor),
    10782, uint(Qt::UpArrowCursor),
    10796, uint(Qt::CrossCursor),
    10808, uint(Qt::WaitCursor),
    10819, uint(Qt::IBeamCursor),
    10831, uint(Qt::SizeVerCursor),
    10845, uint(Qt::SizeHorCursor),
    10859, uint(Qt::SizeBDiagCursor),
    10875, uint(Qt::SizeFDiagCursor),
    10891, uint(Qt::SizeAllCursor),
    10905, uint(Qt::BlankCursor),
    10917, uint(Qt::SplitVCursor),
    10930, uint(Qt::SplitHCursor),
    10943, uint(Qt::PointingHandCursor),
    10962, uint(Qt::ForbiddenCursor),
    10978, uint(Qt::WhatsThisCursor),
    10994, uint(Qt::BusyCursor),
    11005, uint(Qt::OpenHandCursor),
    11020, uint(Qt::ClosedHandCursor),
    11037, uint(Qt::DragCopyCursor),
    11052, uint(Qt::DragMoveCursor),
    11067, uint(Qt::DragLinkCursor),
    11082, uint(Qt::LastCursor),
    11093, uint(Qt::BitmapCursor),
    11106, uint(Qt::CustomCursor),
    11119, uint(Qt::PlainText),
    11129, uint(Qt::RichText),
    11138, uint(Qt::AutoText),
    11147, uint(Qt::LogText),
    11155, uint(Qt::IgnoreAspectRatio),
    11173, uint(Qt::KeepAspectRatio),
    11189, uint(Qt::KeepAspectRatioByExpanding),
    11216, uint(Qt::LeftDockWidgetArea),
    11235, uint(Qt::RightDockWidgetArea),
    11255, uint(Qt::TopDockWidgetArea),
    11273, uint(Qt::BottomDockWidgetArea),
    11294, uint(Qt::DockWidgetArea_Mask),
    11314, uint(Qt::AllDockWidgetAreas),
    11333, uint(Qt::NoDockWidgetArea),
    11216, uint(Qt::LeftDockWidgetArea),
    11235, uint(Qt::RightDockWidgetArea),
    11255, uint(Qt::TopDockWidgetArea),
    11273, uint(Qt::BottomDockWidgetArea),
    11294, uint(Qt::DockWidgetArea_Mask),
    11314, uint(Qt::AllDockWidgetAreas),
    11333, uint(Qt::NoDockWidgetArea),
    11350, uint(Qt::LeftToolBarArea),
    11366, uint(Qt::RightToolBarArea),
    11383, uint(Qt::TopToolBarArea),
    11398, uint(Qt::BottomToolBarArea),
    11416, uint(Qt::ToolBarArea_Mask),
    11433, uint(Qt::AllToolBarAreas),
    11449, uint(Qt::NoToolBarArea),
    11350, uint(Qt::LeftToolBarArea),
    11366, uint(Qt::RightToolBarArea),
    11383, uint(Qt::TopToolBarArea),
    11398, uint(Qt::BottomToolBarArea),
    11416, uint(Qt::ToolBarArea_Mask),
    11433, uint(Qt::AllToolBarAreas),
    11449, uint(Qt::NoToolBarArea),
    11463, uint(Qt::TextDate),
    11472, uint(Qt::ISODate),
    11480, uint(Qt::SystemLocaleDate),
    11497, uint(Qt::LocalDate),
    11507, uint(Qt::LocaleDate),
    11518, uint(Qt::SystemLocaleShortDate),
    11540, uint(Qt::SystemLocaleLongDate),
    11561, uint(Qt::DefaultLocaleShortDate),
    11584, uint(Qt::DefaultLocaleLongDate),
    11606, uint(Qt::LocalTime),
    11616, uint(Qt::UTC),
    11620, uint(Qt::OffsetFromUTC),
    11634, uint(Qt::Monday),
    11641, uint(Qt::Tuesday),
    11649, uint(Qt::Wednesday),
    11659, uint(Qt::Thursday),
    11668, uint(Qt::Friday),
    11675, uint(Qt::Saturday),
    11684, uint(Qt::Sunday),
    11691, uint(Qt::ScrollBarAsNeeded),
    11709, uint(Qt::ScrollBarAlwaysOff),
    11728, uint(Qt::ScrollBarAlwaysOn),
    11746, uint(Qt::CaseInsensitive),
    11762, uint(Qt::CaseSensitive),
    11776, uint(Qt::TopLeftCorner),
    11790, uint(Qt::TopRightCorner),
    11805, uint(Qt::BottomLeftCorner),
    11822, uint(Qt::BottomRightCorner),
    11840, uint(Qt::AutoConnection),
    11855, uint(Qt::DirectConnection),
    11872, uint(Qt::QueuedConnection),
    11889, uint(Qt::AutoCompatConnection),
    11910, uint(Qt::BlockingQueuedConnection),
    11935, uint(Qt::UniqueConnection),
    11952, uint(Qt::WidgetShortcut),
    11967, uint(Qt::WindowShortcut),
    11982, uint(Qt::ApplicationShortcut),
    12002, uint(Qt::WidgetWithChildrenShortcut),
    12029, uint(Qt::OddEvenFill),
    12041, uint(Qt::WindingFill),
    12053, uint(Qt::MaskInColor),
    12065, uint(Qt::MaskOutColor),
    12078, uint(Qt::NoClip),
    12085, uint(Qt::ReplaceClip),
    12097, uint(Qt::IntersectClip),
    12111, uint(Qt::UniteClip),
    12121, uint(Qt::ContainsItemShape),
    12139, uint(Qt::IntersectsItemShape),
    12159, uint(Qt::ContainsItemBoundingRect),
    12184, uint(Qt::IntersectsItemBoundingRect),
    12211, uint(Qt::FastTransformation),
    12230, uint(Qt::SmoothTransformation),
    12251, uint(Qt::XAxis),
    12257, uint(Qt::YAxis),
    12263, uint(Qt::ZAxis),
    12269, uint(Qt::NoContextMenu),
    12283, uint(Qt::DefaultContextMenu),
    12302, uint(Qt::ActionsContextMenu),
    12321, uint(Qt::CustomContextMenu),
    12339, uint(Qt::PreventContextMenu),
    12358, uint(Qt::ImhNone),
    12366, uint(Qt::ImhHiddenText),
    12380, uint(Qt::ImhNoAutoUppercase),
    12399, uint(Qt::ImhPreferNumbers),
    12416, uint(Qt::ImhPreferUppercase),
    12435, uint(Qt::ImhPreferLowercase),
    12454, uint(Qt::ImhNoPredictiveText),
    12474, uint(Qt::ImhDigitsOnly),
    12488, uint(Qt::ImhFormattedNumbersOnly),
    12512, uint(Qt::ImhUppercaseOnly),
    12529, uint(Qt::ImhLowercaseOnly),
    12546, uint(Qt::ImhDialableCharactersOnly),
    12572, uint(Qt::ImhEmailCharactersOnly),
    12595, uint(Qt::ImhUrlCharactersOnly),
    12616, uint(Qt::ImhExclusiveInputMask),
    12358, uint(Qt::ImhNone),
    12366, uint(Qt::ImhHiddenText),
    12380, uint(Qt::ImhNoAutoUppercase),
    12399, uint(Qt::ImhPreferNumbers),
    12416, uint(Qt::ImhPreferUppercase),
    12435, uint(Qt::ImhPreferLowercase),
    12454, uint(Qt::ImhNoPredictiveText),
    12474, uint(Qt::ImhDigitsOnly),
    12488, uint(Qt::ImhFormattedNumbersOnly),
    12512, uint(Qt::ImhUppercaseOnly),
    12529, uint(Qt::ImhLowercaseOnly),
    12546, uint(Qt::ImhDialableCharactersOnly),
    12572, uint(Qt::ImhEmailCharactersOnly),
    12595, uint(Qt::ImhUrlCharactersOnly),
    12616, uint(Qt::ImhExclusiveInputMask),
    12638, uint(Qt::ToolButtonIconOnly),
    12657, uint(Qt::ToolButtonTextOnly),
    12676, uint(Qt::ToolButtonTextBesideIcon),
    12701, uint(Qt::ToolButtonTextUnderIcon),
    12725, uint(Qt::ToolButtonFollowStyle),
    12747, uint(Qt::LeftToRight),
    12759, uint(Qt::RightToLeft),
    12771, uint(Qt::LayoutDirectionAuto),
    12791, uint(Qt::CopyAction),
    12802, uint(Qt::MoveAction),
    12813, uint(Qt::LinkAction),
    12824, uint(Qt::ActionMask),
    12835, uint(Qt::TargetMoveAction),
    12852, uint(Qt::IgnoreAction),
    12791, uint(Qt::CopyAction),
    12802, uint(Qt::MoveAction),
    12813, uint(Qt::LinkAction),
    12824, uint(Qt::ActionMask),
    12835, uint(Qt::TargetMoveAction),
    12852, uint(Qt::IgnoreAction),
    12865, uint(Qt::Unchecked),
    12875, uint(Qt::PartiallyChecked),
    12892, uint(Qt::Checked),
    12900, uint(Qt::NoItemFlags),
    12912, uint(Qt::ItemIsSelectable),
    12929, uint(Qt::ItemIsEditable),
    12944, uint(Qt::ItemIsDragEnabled),
    12962, uint(Qt::ItemIsDropEnabled),
    12980, uint(Qt::ItemIsUserCheckable),
    13000, uint(Qt::ItemIsEnabled),
    13014, uint(Qt::ItemIsTristate),
    13029, uint(Qt::MatchExactly),
    13042, uint(Qt::MatchContains),
    13056, uint(Qt::MatchStartsWith),
    13072, uint(Qt::MatchEndsWith),
    13086, uint(Qt::MatchRegExp),
    13098, uint(Qt::MatchWildcard),
    13112, uint(Qt::MatchFixedString),
    13129, uint(Qt::MatchCaseSensitive),
    13148, uint(Qt::MatchWrap),
    13158, uint(Qt::MatchRecursive),
    13173, uint(Qt::NonModal),
    13182, uint(Qt::WindowModal),
    13194, uint(Qt::ApplicationModal),
    13211, uint(Qt::NoTextInteraction),
    13229, uint(Qt::TextSelectableByMouse),
    13251, uint(Qt::TextSelectableByKeyboard),
    13276, uint(Qt::LinksAccessibleByMouse),
    13299, uint(Qt::LinksAccessibleByKeyboard),
    13325, uint(Qt::TextEditable),
    13338, uint(Qt::TextEditorInteraction),
    13360, uint(Qt::TextBrowserInteraction),
    13211, uint(Qt::NoTextInteraction),
    13229, uint(Qt::TextSelectableByMouse),
    13251, uint(Qt::TextSelectableByKeyboard),
    13276, uint(Qt::LinksAccessibleByMouse),
    13299, uint(Qt::LinksAccessibleByKeyboard),
    13325, uint(Qt::TextEditable),
    13338, uint(Qt::TextEditorInteraction),
    13360, uint(Qt::TextBrowserInteraction),
    13383, uint(Qt::MinimumSize),
    13395, uint(Qt::PreferredSize),
    13409, uint(Qt::MaximumSize),
    13421, uint(Qt::MinimumDescent),
    13436, uint(Qt::NSizeHints),
    13447, uint(Qt::NoGesture),
    13457, uint(Qt::GestureStarted),
    13472, uint(Qt::GestureUpdated),
    13487, uint(Qt::GestureFinished),
    13503, uint(Qt::GestureCanceled),
    13519, uint(Qt::TapGesture),
    13530, uint(Qt::TapAndHoldGesture),
    13548, uint(Qt::PanGesture),
    13559, uint(Qt::PinchGesture),
    13572, uint(Qt::SwipeGesture),
    13585, uint(Qt::CustomGesture),
    13599, uint(Qt::LastGestureType),

       0        // eod
};

static const char qt_meta_stringdata_Qt[] = {
    "Qt\0GlobalColor\0KeyboardModifiers\0"
    "MouseButtons\0Orientation\0Orientations\0"
    "FocusPolicy\0SortOrder\0Alignment\0"
    "TextElideMode\0WindowType\0WindowFlags\0"
    "WindowState\0WindowStates\0WidgetAttribute\0"
    "ApplicationAttribute\0ImageConversionFlags\0"
    "BGMode\0Key\0ArrowType\0PenStyle\0PenCapStyle\0"
    "PenJoinStyle\0BrushStyle\0SizeMode\0"
    "CursorShape\0TextFormat\0AspectRatioMode\0"
    "DockWidgetArea\0DockWidgetAreas\0"
    "ToolBarArea\0ToolBarAreas\0DateFormat\0"
    "TimeSpec\0DayOfWeek\0ScrollBarPolicy\0"
    "CaseSensitivity\0Corner\0ConnectionType\0"
    "ShortcutContext\0FillRule\0MaskMode\0"
    "ClipOperation\0ItemSelectionMode\0"
    "TransformationMode\0Axis\0ContextMenuPolicy\0"
    "InputMethodHint\0InputMethodHints\0"
    "ToolButtonStyle\0LayoutDirection\0"
    "DropAction\0DropActions\0CheckState\0"
    "ItemFlags\0MatchFlags\0WindowModality\0"
    "TextInteractionFlag\0TextInteractionFlags\0"
    "SizeHint\0GestureState\0GestureType\0"
    "color0\0color1\0black\0white\0darkGray\0"
    "gray\0lightGray\0red\0green\0blue\0cyan\0"
    "magenta\0yellow\0darkRed\0darkGreen\0"
    "darkBlue\0darkCyan\0darkMagenta\0darkYellow\0"
    "transparent\0NoModifier\0ShiftModifier\0"
    "ControlModifier\0AltModifier\0MetaModifier\0"
    "KeypadModifier\0GroupSwitchModifier\0"
    "KeyboardModifierMask\0NoButton\0LeftButton\0"
    "RightButton\0MidButton\0MiddleButton\0"
    "XButton1\0XButton2\0MouseButtonMask\0"
    "Horizontal\0Vertical\0NoFocus\0TabFocus\0"
    "ClickFocus\0StrongFocus\0WheelFocus\0"
    "AscendingOrder\0DescendingOrder\0AlignLeft\0"
    "AlignLeading\0AlignRight\0AlignTrailing\0"
    "AlignHCenter\0AlignJustify\0AlignAbsolute\0"
    "AlignHorizontal_Mask\0AlignTop\0AlignBottom\0"
    "AlignVCenter\0AlignVertical_Mask\0"
    "AlignCenter\0ElideLeft\0ElideRight\0"
    "ElideMiddle\0ElideNone\0Widget\0Window\0"
    "Dialog\0Sheet\0Drawer\0Popup\0Tool\0ToolTip\0"
    "SplashScreen\0Desktop\0SubWindow\0"
    "WindowType_Mask\0MSWindowsFixedSizeDialogHint\0"
    "MSWindowsOwnDC\0X11BypassWindowManagerHint\0"
    "FramelessWindowHint\0WindowTitleHint\0"
    "WindowSystemMenuHint\0WindowMinimizeButtonHint\0"
    "WindowMaximizeButtonHint\0"
    "WindowMinMaxButtonsHint\0"
    "WindowContextHelpButtonHint\0"
    "WindowShadeButtonHint\0WindowStaysOnTopHint\0"
    "CustomizeWindowHint\0WindowStaysOnBottomHint\0"
    "WindowCloseButtonHint\0MacWindowToolBarButtonHint\0"
    "BypassGraphicsProxyWidget\0WindowOkButtonHint\0"
    "WindowCancelButtonHint\0WindowSoftkeysVisibleHint\0"
    "WindowSoftkeysRespondHint\0WindowNoState\0"
    "WindowMinimized\0WindowMaximized\0"
    "WindowFullScreen\0WindowActive\0WA_Disabled\0"
    "WA_UnderMouse\0WA_MouseTracking\0"
    "WA_ContentsPropagated\0WA_OpaquePaintEvent\0"
    "WA_NoBackground\0WA_StaticContents\0"
    "WA_LaidOut\0WA_PaintOnScreen\0"
    "WA_NoSystemBackground\0WA_UpdatesDisabled\0"
    "WA_Mapped\0WA_MacNoClickThrough\0"
    "WA_PaintOutsidePaintEvent\0"
    "WA_InputMethodEnabled\0WA_WState_Visible\0"
    "WA_WState_Hidden\0WA_ForceDisabled\0"
    "WA_KeyCompression\0WA_PendingMoveEvent\0"
    "WA_PendingResizeEvent\0WA_SetPalette\0"
    "WA_SetFont\0WA_SetCursor\0"
    "WA_NoChildEventsFromChildren\0"
    "WA_WindowModified\0WA_Resized\0WA_Moved\0"
    "WA_PendingUpdate\0WA_InvalidSize\0"
    "WA_MacBrushedMetal\0WA_MacMetalStyle\0"
    "WA_CustomWhatsThis\0WA_LayoutOnEntireRect\0"
    "WA_OutsideWSRange\0WA_GrabbedShortcut\0"
    "WA_TransparentForMouseEvents\0"
    "WA_PaintUnclipped\0WA_SetWindowIcon\0"
    "WA_NoMouseReplay\0WA_DeleteOnClose\0"
    "WA_RightToLeft\0WA_SetLayoutDirection\0"
    "WA_NoChildEventsForParent\0"
    "WA_ForceUpdatesDisabled\0WA_WState_Created\0"
    "WA_WState_CompressKeys\0WA_WState_InPaintEvent\0"
    "WA_WState_Reparented\0WA_WState_ConfigPending\0"
    "WA_WState_Polished\0WA_WState_DND\0"
    "WA_WState_OwnSizePolicy\0"
    "WA_WState_ExplicitShowHide\0WA_ShowModal\0"
    "WA_MouseNoMask\0WA_GroupLeader\0"
    "WA_NoMousePropagation\0WA_Hover\0"
    "WA_InputMethodTransparent\0WA_QuitOnClose\0"
    "WA_KeyboardFocusChange\0WA_AcceptDrops\0"
    "WA_DropSiteRegistered\0WA_ForceAcceptDrops\0"
    "WA_WindowPropagation\0WA_NoX11EventCompression\0"
    "WA_TintedBackground\0WA_X11OpenGLOverlay\0"
    "WA_AlwaysShowToolTips\0WA_MacOpaqueSizeGrip\0"
    "WA_SetStyle\0WA_SetLocale\0WA_MacShowFocusRect\0"
    "WA_MacNormalSize\0WA_MacSmallSize\0"
    "WA_MacMiniSize\0WA_LayoutUsesWidgetRect\0"
    "WA_StyledBackground\0WA_MSWindowsUseDirect3D\0"
    "WA_CanHostQMdiSubWindowTitleBar\0"
    "WA_MacAlwaysShowToolWindow\0WA_StyleSheet\0"
    "WA_ShowWithoutActivating\0"
    "WA_X11BypassTransientForHint\0"
    "WA_NativeWindow\0WA_DontCreateNativeAncestors\0"
    "WA_MacVariableSize\0WA_DontShowOnScreen\0"
    "WA_X11NetWmWindowTypeDesktop\0"
    "WA_X11NetWmWindowTypeDock\0"
    "WA_X11NetWmWindowTypeToolBar\0"
    "WA_X11NetWmWindowTypeMenu\0"
    "WA_X11NetWmWindowTypeUtility\0"
    "WA_X11NetWmWindowTypeSplash\0"
    "WA_X11NetWmWindowTypeDialog\0"
    "WA_X11NetWmWindowTypeDropDownMenu\0"
    "WA_X11NetWmWindowTypePopupMenu\0"
    "WA_X11NetWmWindowTypeToolTip\0"
    "WA_X11NetWmWindowTypeNotification\0"
    "WA_X11NetWmWindowTypeCombo\0"
    "WA_X11NetWmWindowTypeDND\0WA_MacFrameworkScaled\0"
    "WA_SetWindowModality\0WA_WState_WindowOpacitySet\0"
    "WA_TranslucentBackground\0WA_AcceptTouchEvents\0"
    "WA_WState_AcceptedTouchBeginEvent\0"
    "WA_TouchPadAcceptSingleTouchEvents\0"
    "WA_MergeSoftkeys\0WA_MergeSoftkeysRecursively\0"
    "WA_LockPortraitOrientation\0"
    "WA_LockLandscapeOrientation\0"
    "WA_AutoOrientation\0WA_X11DoNotAcceptFocus\0"
    "WA_SymbianNoSystemRotation\0WA_AttributeCount\0"
    "AA_ImmediateWidgetCreation\0"
    "AA_MSWindowsUseDirect3DByDefault\0"
    "AA_DontShowIconsInMenus\0AA_NativeWindows\0"
    "AA_DontCreateNativeWidgetSiblings\0"
    "AA_MacPluginApplication\0AA_DontUseNativeMenuBar\0"
    "AA_MacDontSwapCtrlAndMeta\0"
    "AA_S60DontConstructApplicationPanes\0"
    "AA_S60DisablePartialScreenInputMode\0"
    "AA_AttributeCount\0ColorMode_Mask\0"
    "AutoColor\0ColorOnly\0MonoOnly\0"
    "AlphaDither_Mask\0ThresholdAlphaDither\0"
    "OrderedAlphaDither\0DiffuseAlphaDither\0"
    "NoAlpha\0Dither_Mask\0DiffuseDither\0"
    "OrderedDither\0ThresholdDither\0"
    "DitherMode_Mask\0AutoDither\0PreferDither\0"
    "AvoidDither\0NoOpaqueDetection\0"
    "NoFormatConversion\0TransparentMode\0"
    "OpaqueMode\0Key_Escape\0Key_Tab\0Key_Backtab\0"
    "Key_Backspace\0Key_Return\0Key_Enter\0"
    "Key_Insert\0Key_Delete\0Key_Pause\0"
    "Key_Print\0Key_SysReq\0Key_Clear\0Key_Home\0"
    "Key_End\0Key_Left\0Key_Up\0Key_Right\0"
    "Key_Down\0Key_PageUp\0Key_PageDown\0"
    "Key_Shift\0Key_Control\0Key_Meta\0Key_Alt\0"
    "Key_CapsLock\0Key_NumLock\0Key_ScrollLock\0"
    "Key_F1\0Key_F2\0Key_F3\0Key_F4\0Key_F5\0"
    "Key_F6\0Key_F7\0Key_F8\0Key_F9\0Key_F10\0"
    "Key_F11\0Key_F12\0Key_F13\0Key_F14\0Key_F15\0"
    "Key_F16\0Key_F17\0Key_F18\0Key_F19\0Key_F20\0"
    "Key_F21\0Key_F22\0Key_F23\0Key_F24\0Key_F25\0"
    "Key_F26\0Key_F27\0Key_F28\0Key_F29\0Key_F30\0"
    "Key_F31\0Key_F32\0Key_F33\0Key_F34\0Key_F35\0"
    "Key_Super_L\0Key_Super_R\0Key_Menu\0"
    "Key_Hyper_L\0Key_Hyper_R\0Key_Help\0"
    "Key_Direction_L\0Key_Direction_R\0"
    "Key_Space\0Key_Any\0Key_Exclam\0Key_QuoteDbl\0"
    "Key_NumberSign\0Key_Dollar\0Key_Percent\0"
    "Key_Ampersand\0Key_Apostrophe\0Key_ParenLeft\0"
    "Key_ParenRight\0Key_Asterisk\0Key_Plus\0"
    "Key_Comma\0Key_Minus\0Key_Period\0Key_Slash\0"
    "Key_0\0Key_1\0Key_2\0Key_3\0Key_4\0Key_5\0"
    "Key_6\0Key_7\0Key_8\0Key_9\0Key_Colon\0"
    "Key_Semicolon\0Key_Less\0Key_Equal\0"
    "Key_Greater\0Key_Question\0Key_At\0Key_A\0"
    "Key_B\0Key_C\0Key_D\0Key_E\0Key_F\0Key_G\0"
    "Key_H\0Key_I\0Key_J\0Key_K\0Key_L\0Key_M\0"
    "Key_N\0Key_O\0Key_P\0Key_Q\0Key_R\0Key_S\0"
    "Key_T\0Key_U\0Key_V\0Key_W\0Key_X\0Key_Y\0"
    "Key_Z\0Key_BracketLeft\0Key_Backslash\0"
    "Key_BracketRight\0Key_AsciiCircum\0"
    "Key_Underscore\0Key_QuoteLeft\0Key_BraceLeft\0"
    "Key_Bar\0Key_BraceRight\0Key_AsciiTilde\0"
    "Key_nobreakspace\0Key_exclamdown\0"
    "Key_cent\0Key_sterling\0Key_currency\0"
    "Key_yen\0Key_brokenbar\0Key_section\0"
    "Key_diaeresis\0Key_copyright\0Key_ordfeminine\0"
    "Key_guillemotleft\0Key_notsign\0Key_hyphen\0"
    "Key_registered\0Key_macron\0Key_degree\0"
    "Key_plusminus\0Key_twosuperior\0"
    "Key_threesuperior\0Key_acute\0Key_mu\0"
    "Key_paragraph\0Key_periodcentered\0"
    "Key_cedilla\0Key_onesuperior\0Key_masculine\0"
    "Key_guillemotright\0Key_onequarter\0"
    "Key_onehalf\0Key_threequarters\0"
    "Key_questiondown\0Key_Agrave\0Key_Aacute\0"
    "Key_Acircumflex\0Key_Atilde\0Key_Adiaeresis\0"
    "Key_Aring\0Key_AE\0Key_Ccedilla\0Key_Egrave\0"
    "Key_Eacute\0Key_Ecircumflex\0Key_Ediaeresis\0"
    "Key_Igrave\0Key_Iacute\0Key_Icircumflex\0"
    "Key_Idiaeresis\0Key_ETH\0Key_Ntilde\0"
    "Key_Ograve\0Key_Oacute\0Key_Ocircumflex\0"
    "Key_Otilde\0Key_Odiaeresis\0Key_multiply\0"
    "Key_Ooblique\0Key_Ugrave\0Key_Uacute\0"
    "Key_Ucircumflex\0Key_Udiaeresis\0"
    "Key_Yacute\0Key_THORN\0Key_ssharp\0"
    "Key_division\0Key_ydiaeresis\0Key_AltGr\0"
    "Key_Multi_key\0Key_Codeinput\0"
    "Key_SingleCandidate\0Key_MultipleCandidate\0"
    "Key_PreviousCandidate\0Key_Mode_switch\0"
    "Key_Kanji\0Key_Muhenkan\0Key_Henkan\0"
    "Key_Romaji\0Key_Hiragana\0Key_Katakana\0"
    "Key_Hiragana_Katakana\0Key_Zenkaku\0"
    "Key_Hankaku\0Key_Zenkaku_Hankaku\0"
    "Key_Touroku\0Key_Massyo\0Key_Kana_Lock\0"
    "Key_Kana_Shift\0Key_Eisu_Shift\0"
    "Key_Eisu_toggle\0Key_Hangul\0Key_Hangul_Start\0"
    "Key_Hangul_End\0Key_Hangul_Hanja\0"
    "Key_Hangul_Jamo\0Key_Hangul_Romaja\0"
    "Key_Hangul_Jeonja\0Key_Hangul_Banja\0"
    "Key_Hangul_PreHanja\0Key_Hangul_PostHanja\0"
    "Key_Hangul_Special\0Key_Dead_Grave\0"
    "Key_Dead_Acute\0Key_Dead_Circumflex\0"
    "Key_Dead_Tilde\0Key_Dead_Macron\0"
    "Key_Dead_Breve\0Key_Dead_Abovedot\0"
    "Key_Dead_Diaeresis\0Key_Dead_Abovering\0"
    "Key_Dead_Doubleacute\0Key_Dead_Caron\0"
    "Key_Dead_Cedilla\0Key_Dead_Ogonek\0"
    "Key_Dead_Iota\0Key_Dead_Voiced_Sound\0"
    "Key_Dead_Semivoiced_Sound\0Key_Dead_Belowdot\0"
    "Key_Dead_Hook\0Key_Dead_Horn\0Key_Back\0"
    "Key_Forward\0Key_Stop\0Key_Refresh\0"
    "Key_VolumeDown\0Key_VolumeMute\0"
    "Key_VolumeUp\0Key_BassBoost\0Key_BassUp\0"
    "Key_BassDown\0Key_TrebleUp\0Key_TrebleDown\0"
    "Key_MediaPlay\0Key_MediaStop\0"
    "Key_MediaPrevious\0Key_MediaNext\0"
    "Key_MediaRecord\0Key_MediaPause\0"
    "Key_MediaTogglePlayPause\0Key_HomePage\0"
    "Key_Favorites\0Key_Search\0Key_Standby\0"
    "Key_OpenUrl\0Key_LaunchMail\0Key_LaunchMedia\0"
    "Key_Launch0\0Key_Launch1\0Key_Launch2\0"
    "Key_Launch3\0Key_Launch4\0Key_Launch5\0"
    "Key_Launch6\0Key_Launch7\0Key_Launch8\0"
    "Key_Launch9\0Key_LaunchA\0Key_LaunchB\0"
    "Key_LaunchC\0Key_LaunchD\0Key_LaunchE\0"
    "Key_LaunchF\0Key_MonBrightnessUp\0"
    "Key_MonBrightnessDown\0Key_KeyboardLightOnOff\0"
    "Key_KeyboardBrightnessUp\0"
    "Key_KeyboardBrightnessDown\0Key_PowerOff\0"
    "Key_WakeUp\0Key_Eject\0Key_ScreenSaver\0"
    "Key_WWW\0Key_Memo\0Key_LightBulb\0Key_Shop\0"
    "Key_History\0Key_AddFavorite\0Key_HotLinks\0"
    "Key_BrightnessAdjust\0Key_Finance\0"
    "Key_Community\0Key_AudioRewind\0"
    "Key_BackForward\0Key_ApplicationLeft\0"
    "Key_ApplicationRight\0Key_Book\0Key_CD\0"
    "Key_Calculator\0Key_ToDoList\0Key_ClearGrab\0"
    "Key_Close\0Key_Copy\0Key_Cut\0Key_Display\0"
    "Key_DOS\0Key_Documents\0Key_Excel\0"
    "Key_Explorer\0Key_Game\0Key_Go\0Key_iTouch\0"
    "Key_LogOff\0Key_Market\0Key_Meeting\0"
    "Key_MenuKB\0Key_MenuPB\0Key_MySites\0"
    "Key_News\0Key_OfficeHome\0Key_Option\0"
    "Key_Paste\0Key_Phone\0Key_Calendar\0"
    "Key_Reply\0Key_Reload\0Key_RotateWindows\0"
    "Key_RotationPB\0Key_RotationKB\0Key_Save\0"
    "Key_Send\0Key_Spell\0Key_SplitScreen\0"
    "Key_Support\0Key_TaskPane\0Key_Terminal\0"
    "Key_Tools\0Key_Travel\0Key_Video\0Key_Word\0"
    "Key_Xfer\0Key_ZoomIn\0Key_ZoomOut\0"
    "Key_Away\0Key_Messenger\0Key_WebCam\0"
    "Key_MailForward\0Key_Pictures\0Key_Music\0"
    "Key_Battery\0Key_Bluetooth\0Key_WLAN\0"
    "Key_UWB\0Key_AudioForward\0Key_AudioRepeat\0"
    "Key_AudioRandomPlay\0Key_Subtitle\0"
    "Key_AudioCycleTrack\0Key_Time\0Key_Hibernate\0"
    "Key_View\0Key_TopMenu\0Key_PowerDown\0"
    "Key_Suspend\0Key_ContrastAdjust\0"
    "Key_LaunchG\0Key_LaunchH\0Key_MediaLast\0"
    "Key_Select\0Key_Yes\0Key_No\0Key_Cancel\0"
    "Key_Printer\0Key_Execute\0Key_Sleep\0"
    "Key_Play\0Key_Zoom\0Key_Context1\0"
    "Key_Context2\0Key_Context3\0Key_Context4\0"
    "Key_Call\0Key_Hangup\0Key_Flip\0"
    "Key_ToggleCallHangup\0Key_VoiceDial\0"
    "Key_LastNumberRedial\0Key_Camera\0"
    "Key_CameraFocus\0Key_unknown\0NoArrow\0"
    "UpArrow\0DownArrow\0LeftArrow\0RightArrow\0"
    "NoPen\0SolidLine\0DashLine\0DotLine\0"
    "DashDotLine\0DashDotDotLine\0CustomDashLine\0"
    "FlatCap\0SquareCap\0RoundCap\0MPenCapStyle\0"
    "MiterJoin\0BevelJoin\0RoundJoin\0"
    "SvgMiterJoin\0MPenJoinStyle\0NoBrush\0"
    "SolidPattern\0Dense1Pattern\0Dense2Pattern\0"
    "Dense3Pattern\0Dense4Pattern\0Dense5Pattern\0"
    "Dense6Pattern\0Dense7Pattern\0HorPattern\0"
    "VerPattern\0CrossPattern\0BDiagPattern\0"
    "FDiagPattern\0DiagCrossPattern\0"
    "LinearGradientPattern\0RadialGradientPattern\0"
    "ConicalGradientPattern\0TexturePattern\0"
    "AbsoluteSize\0RelativeSize\0ArrowCursor\0"
    "UpArrowCursor\0CrossCursor\0WaitCursor\0"
    "IBeamCursor\0SizeVerCursor\0SizeHorCursor\0"
    "SizeBDiagCursor\0SizeFDiagCursor\0"
    "SizeAllCursor\0BlankCursor\0SplitVCursor\0"
    "SplitHCursor\0PointingHandCursor\0"
    "ForbiddenCursor\0WhatsThisCursor\0"
    "BusyCursor\0OpenHandCursor\0ClosedHandCursor\0"
    "DragCopyCursor\0DragMoveCursor\0"
    "DragLinkCursor\0LastCursor\0BitmapCursor\0"
    "CustomCursor\0PlainText\0RichText\0"
    "AutoText\0LogText\0IgnoreAspectRatio\0"
    "KeepAspectRatio\0KeepAspectRatioByExpanding\0"
    "LeftDockWidgetArea\0RightDockWidgetArea\0"
    "TopDockWidgetArea\0BottomDockWidgetArea\0"
    "DockWidgetArea_Mask\0AllDockWidgetAreas\0"
    "NoDockWidgetArea\0LeftToolBarArea\0"
    "RightToolBarArea\0TopToolBarArea\0"
    "BottomToolBarArea\0ToolBarArea_Mask\0"
    "AllToolBarAreas\0NoToolBarArea\0TextDate\0"
    "ISODate\0SystemLocaleDate\0LocalDate\0"
    "LocaleDate\0SystemLocaleShortDate\0"
    "SystemLocaleLongDate\0DefaultLocaleShortDate\0"
    "DefaultLocaleLongDate\0LocalTime\0UTC\0"
    "OffsetFromUTC\0Monday\0Tuesday\0Wednesday\0"
    "Thursday\0Friday\0Saturday\0Sunday\0"
    "ScrollBarAsNeeded\0ScrollBarAlwaysOff\0"
    "ScrollBarAlwaysOn\0CaseInsensitive\0"
    "CaseSensitive\0TopLeftCorner\0TopRightCorner\0"
    "BottomLeftCorner\0BottomRightCorner\0"
    "AutoConnection\0DirectConnection\0"
    "QueuedConnection\0AutoCompatConnection\0"
    "BlockingQueuedConnection\0UniqueConnection\0"
    "WidgetShortcut\0WindowShortcut\0"
    "ApplicationShortcut\0WidgetWithChildrenShortcut\0"
    "OddEvenFill\0WindingFill\0MaskInColor\0"
    "MaskOutColor\0NoClip\0ReplaceClip\0"
    "IntersectClip\0UniteClip\0ContainsItemShape\0"
    "IntersectsItemShape\0ContainsItemBoundingRect\0"
    "IntersectsItemBoundingRect\0"
    "FastTransformation\0SmoothTransformation\0"
    "XAxis\0YAxis\0ZAxis\0NoContextMenu\0"
    "DefaultContextMenu\0ActionsContextMenu\0"
    "CustomContextMenu\0PreventContextMenu\0"
    "ImhNone\0ImhHiddenText\0ImhNoAutoUppercase\0"
    "ImhPreferNumbers\0ImhPreferUppercase\0"
    "ImhPreferLowercase\0ImhNoPredictiveText\0"
    "ImhDigitsOnly\0ImhFormattedNumbersOnly\0"
    "ImhUppercaseOnly\0ImhLowercaseOnly\0"
    "ImhDialableCharactersOnly\0"
    "ImhEmailCharactersOnly\0ImhUrlCharactersOnly\0"
    "ImhExclusiveInputMask\0ToolButtonIconOnly\0"
    "ToolButtonTextOnly\0ToolButtonTextBesideIcon\0"
    "ToolButtonTextUnderIcon\0ToolButtonFollowStyle\0"
    "LeftToRight\0RightToLeft\0LayoutDirectionAuto\0"
    "CopyAction\0MoveAction\0LinkAction\0"
    "ActionMask\0TargetMoveAction\0IgnoreAction\0"
    "Unchecked\0PartiallyChecked\0Checked\0"
    "NoItemFlags\0ItemIsSelectable\0"
    "ItemIsEditable\0ItemIsDragEnabled\0"
    "ItemIsDropEnabled\0ItemIsUserCheckable\0"
    "ItemIsEnabled\0ItemIsTristate\0MatchExactly\0"
    "MatchContains\0MatchStartsWith\0"
    "MatchEndsWith\0MatchRegExp\0MatchWildcard\0"
    "MatchFixedString\0MatchCaseSensitive\0"
    "MatchWrap\0MatchRecursive\0NonModal\0"
    "WindowModal\0ApplicationModal\0"
    "NoTextInteraction\0TextSelectableByMouse\0"
    "TextSelectableByKeyboard\0"
    "LinksAccessibleByMouse\0LinksAccessibleByKeyboard\0"
    "TextEditable\0TextEditorInteraction\0"
    "TextBrowserInteraction\0MinimumSize\0"
    "PreferredSize\0MaximumSize\0MinimumDescent\0"
    "NSizeHints\0NoGesture\0GestureStarted\0"
    "GestureUpdated\0GestureFinished\0"
    "GestureCanceled\0TapGesture\0TapAndHoldGesture\0"
    "PanGesture\0PinchGesture\0SwipeGesture\0"
    "CustomGesture\0LastGestureType\0"
};

const QMetaObject QObject::staticQtMetaObject = {
    { 0, qt_meta_stringdata_Qt,
      qt_meta_data_Qt, 0 }
};
QT_END_MOC_NAMESPACE
